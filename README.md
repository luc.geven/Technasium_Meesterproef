# Technasium Meesterproef

This is a python project that analyses the inflow, through flow and outflow data of Technasium.

## Getting Started

### Install dependencies

```
pip install -r requirements.txt
```

### Folder Structure
```
+ build: Contains a windows executable
+ docs: Documentation
+ technasium_meesterproef: Contains the python files
+ tests: Package integration and unit tests
+ LICENCE: Laywering up
+ requirements.txt: Development dependencies
+ Setup.py: Package and distribution management
+ Technasium_Meesterproef.xlsm: Defenitive Excel file to collect all data
```

## Documentation

See [the documentation](/docs/documentation.pdf) for details.

## Testing

This project provides a test unit. Type the following to run the test units:
```
python3 -m unittest discover -v
```

## Commit Guideline

See [the commit guideline](COMMIT_GUIDELINE.md) for details.

## Copyright

See [LICENSE](LICENSE) for details.
Copyright (c) 2018 [Luc Geven]
