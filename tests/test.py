import unittest
import pandas as pd

from technasium_meesterproef.helpers import *

class BasicTestSuite(unittest.TestCase):
    def test_helpers(self):
        assert compare_old_to_new(20,34) == 70

if __name__ == '__main__':
    unittest.main()
