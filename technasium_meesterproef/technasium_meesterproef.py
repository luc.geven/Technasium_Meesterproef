'''
A python interface that runs the following modules:
    - Landelijk.py
    - Netwerk.py
    - School.py
'''

import Landelijk
import Netwerk
import School

total_technasium_students_table, landelijk_students_data = Landelijk.main()

total_havo_boys_percentage = (total_technasium_students_table['Totaal havo jongens'].Totaal / total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
total_havo_girls_percentage = (total_technasium_students_table['Totaal havo meisjes'].Totaal / total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
total_vwo_boys_percentage = (total_technasium_students_table['Totaal vwo jongens'].Totaal / total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
total_vwo_girls_percentage = (total_technasium_students_table['Totaal vwo meisjes'].Totaal / total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
total_students_with_cluster_1_2_percentage = (len(landelijk_students_data.loc[landelijk_students_data['cluster'] == '1,2'].index) / len(landelijk_students_data['cluster'].index)) * 100

network_total_technasium_students_table, network_students_data = Netwerk.main(total_havo_boys_percentage, total_havo_girls_percentage, total_vwo_boys_percentage , total_vwo_girls_percentage, total_students_with_cluster_1_2_percentage)

network_total_havo_boys_percentage = (network_total_technasium_students_table['Totaal havo jongens'].Totaal / network_total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
network_total_havo_girls_percentage = (network_total_technasium_students_table['Totaal havo meisjes'].Totaal / network_total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
network_total_vwo_boys_percentage = (network_total_technasium_students_table['Totaal vwo jongens'].Totaal / network_total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
network_total_vwo_girls_percentage = (network_total_technasium_students_table['Totaal vwo meisjes'].Totaal / network_total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
network_total_students_with_cluster_1_2_percentage = (len(network_students_data.loc[network_students_data['cluster'] == '1,2'].index) / len(network_students_data['cluster'].index)) * 100

School.main(total_havo_boys_percentage, total_havo_girls_percentage, total_vwo_boys_percentage , total_vwo_girls_percentage, total_students_with_cluster_1_2_percentage,
            network_total_havo_boys_percentage, network_total_havo_girls_percentage, network_total_vwo_boys_percentage , network_total_vwo_girls_percentage, network_total_students_with_cluster_1_2_percentage)
