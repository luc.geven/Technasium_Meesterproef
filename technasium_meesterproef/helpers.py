import pandas as pd
import matplotlib.pyplot as plt
import fpdf
import markdown2

def get_total_technasium_students_table(data):
    '''
    This function calculates the total students of a school or multiple schools.

    Args:
        data (Pandas.DataFrame): This is the data that will be used for calculating the total students of a school or multiple schools.
    Returns:
        frame (Pandas.Dataframe): This is a created dataframe that provides the results from the calculations structuredly.
    '''
    tot_havo_j_1, tot_havo_j_2, tot_havo_j_3, tot_havo_j_4, tot_havo_j_5 = (0,) *5
    tot_vwo_j_1, tot_vwo_j_2, tot_vwo_j_3, tot_vwo_j_4, tot_vwo_j_5, tot_vwo_j_6 = (0,) *6

    tot_havo_m_1, tot_havo_m_2, tot_havo_m_3, tot_havo_m_4, tot_havo_m_5 = (0,) *5
    tot_vwo_m_1, tot_vwo_m_2, tot_vwo_m_3, tot_vwo_m_4, tot_vwo_m_5, tot_vwo_m_6 = (0,) *6

    tot_ll_1, tot_ll_2, tot_ll_3, tot_ll_4, tot_ll_5, tot_ll_6 = (0,) *6

    for index, row in data.iterrows():
        tot_havo_j_1 += row.havo_j_1
        tot_havo_j_2 += row.havo_j_2
        tot_havo_j_3 += row.havo_j_3
        tot_havo_j_4 += row.havo_j_4
        tot_havo_j_5 += row.havo_j_5

        tot_havo_m_1 += row.havo_m_1
        tot_havo_m_2 += row.havo_m_2
        tot_havo_m_3 += row.havo_m_3
        tot_havo_m_4 += row.havo_m_4
        tot_havo_m_5 += row.havo_m_5

        tot_vwo_j_1 += row.vwo_j_1
        tot_vwo_j_2 += row.vwo_j_2
        tot_vwo_j_3 += row.vwo_j_3
        tot_vwo_j_4 += row.vwo_j_4
        tot_vwo_j_5 += row.vwo_j_5
        tot_vwo_j_6 += row.vwo_j_6

        tot_vwo_m_1 += row.vwo_m_1
        tot_vwo_m_2 += row.vwo_m_2
        tot_vwo_m_3 += row.vwo_m_3
        tot_vwo_m_4 += row.vwo_m_4
        tot_vwo_m_5 += row.vwo_m_5
        tot_vwo_m_6 += row.vwo_m_6

        tot_ll_1 += row.tot_ll_1
        tot_ll_2 += row.tot_ll_2
        tot_ll_3 += row.tot_ll_3
        tot_ll_4 += row.tot_ll_4
        tot_ll_5 += row.tot_ll_5
        tot_ll_6 += row.tot_ll_6

        tot_tech_ll_1 = tot_havo_j_1 + tot_havo_m_1 + tot_vwo_j_1 + tot_vwo_m_1
        tot_tech_ll_2 = tot_havo_j_2 + tot_havo_m_2 + tot_vwo_j_2 + tot_vwo_m_2
        tot_tech_ll_3 = tot_havo_j_3 + tot_havo_m_3 + tot_vwo_j_3 + tot_vwo_m_3
        tot_tech_ll_4 = tot_havo_j_4 + tot_havo_m_4 + tot_vwo_j_4 + tot_vwo_m_4
        tot_tech_ll_5 = tot_havo_j_5 + tot_havo_m_5 + tot_vwo_j_5 + tot_vwo_m_5
        tot_tech_ll_6 = tot_vwo_j_6 + tot_vwo_m_6


    test_data = [
        [tot_havo_j_1, tot_havo_m_1, tot_vwo_j_1, tot_vwo_m_1, tot_tech_ll_1, tot_ll_1],
        [tot_havo_j_2, tot_havo_m_2, tot_vwo_j_2, tot_vwo_m_2, tot_tech_ll_2, tot_ll_2],
        [tot_havo_j_3, tot_havo_m_3, tot_vwo_j_3, tot_vwo_m_3, tot_tech_ll_3, tot_ll_3],
        [tot_havo_j_4, tot_havo_m_4, tot_vwo_j_4, tot_vwo_m_4, tot_tech_ll_4, tot_ll_4],
        [tot_havo_j_5, tot_havo_m_5, tot_vwo_j_5, tot_vwo_m_5, tot_tech_ll_5, tot_ll_5],
        [0, 0, tot_vwo_j_6, tot_vwo_m_6, tot_tech_ll_6, tot_ll_6]
                ]

    frame = pd.DataFrame(data=test_data, columns = ['Totaal havo jongens', 'Totaal havo meisjes', 'Totaal vwo jongens', 'Totaal vwo meisjes', 'Totaal technasium leerlingen', 'Totaal leerlingen'],
                         index=['Leerjaar 1', 'Leerjaar 2', 'Leerjaar 3', 'Leerjaar 4', 'Leerjaar 5', 'Leerjaar 6'])
    frame.loc['Totaal'] = pd.Series(
        [
        frame['Totaal havo jongens'].sum(),
        frame['Totaal havo meisjes'].sum(),
        frame['Totaal vwo jongens'].sum(),
        frame['Totaal vwo meisjes'].sum(),
        frame['Totaal technasium leerlingen'].sum(),
        frame['Totaal leerlingen'].sum()
        ],
        index = ['Totaal havo jongens', 'Totaal havo meisjes', 'Totaal vwo jongens', 'Totaal vwo meisjes','Totaal technasium leerlingen', 'Totaal leerlingen'])

    return frame

def get_student_profiles_table(data):
    '''
    This function calculates how many students do a certain profile in a school or multiple schools.

    Args:
        data (Pandas.DataFrame): This is the data that will be used for calculating how many students do a certain profile in a school or multiple schools.

    Returns:
        frame (Pandas.Dataframe): This is a created dataframe that provides the results from the calculations structuredly.
    '''
    tot_havo_NG_4, tot_havo_NG_5 = (0,) *2
    tot_havo_NT_4, tot_havo_NT_5 = (0,) *2
    tot_havo_NG_NT_4, tot_havo_NG_NT_5 = (0,) *2
    tot_havo_OVERIG_4, tot_havo_OVERIG_5 = (0,) *2

    tot_vwo_NG_4, tot_vwo_NG_5, tot_vwo_NG_6 = (0,) *3
    tot_vwo_NT_4, tot_vwo_NT_5, tot_vwo_NT_6 = (0,) *3
    tot_vwo_NG_NT_4, tot_vwo_NG_NT_5, tot_vwo_NG_NT_6 = (0,) *3
    tot_vwo_OVERIG_4, tot_vwo_OVERIG_5, tot_vwo_OVERIG_6 = (0,) *3

    for index, row in data.iterrows():
        tot_havo_NG_4 += row.havo_NG_4
        tot_havo_NG_5 += row.havo_NG_5
        tot_havo_NT_4 += row.havo_NT_4
        tot_havo_NT_5 += row.havo_NT_5
        tot_havo_NG_NT_4 += row.havo_NG_NT_4
        tot_havo_NG_NT_5 += row.havo_NG_NT_5
        tot_havo_OVERIG_4 += row.havo_OVERIG_4
        tot_havo_OVERIG_5 += row.havo_OVERIG_5

        tot_vwo_NG_4 += row.vwo_NG_4
        tot_vwo_NG_5 += row.vwo_NG_5
        tot_vwo_NG_6 += row.vwo_NG_6
        tot_vwo_NT_4 += row.vwo_NT_4
        tot_vwo_NT_5 += row.vwo_NT_5
        tot_vwo_NT_6 += row.vwo_NT_6
        tot_vwo_NG_NT_4 += row.vwo_NG_NT_4
        tot_vwo_NG_NT_5 += row.vwo_NG_NT_5
        tot_vwo_NG_NT_6 += row.vwo_NG_NT_6
        tot_vwo_OVERIG_4 += row.vwo_OVERIG_4
        tot_vwo_OVERIG_5 += row.vwo_OVERIG_5
        tot_vwo_OVERIG_6 += row.vwo_OVERIG_6

    test_data = [[tot_havo_NG_4, tot_havo_NT_4, tot_havo_NG_NT_4, tot_havo_OVERIG_4],
            [tot_havo_NG_5, tot_havo_NT_5, tot_havo_NG_NT_5, tot_havo_OVERIG_5],
            [tot_vwo_NG_4, tot_vwo_NT_4, tot_vwo_NG_NT_4, tot_vwo_OVERIG_4],
            [tot_vwo_NG_5, tot_vwo_NT_5, tot_vwo_NG_NT_5, tot_vwo_OVERIG_5],
            [tot_vwo_NG_6, tot_vwo_NT_6, tot_vwo_NG_NT_6, tot_vwo_OVERIG_6]]

    frame = pd.DataFrame(data=test_data, columns = ['NG', 'NT', 'NG&NT', 'OVERIG'], index=['Havo 4', 'Havo 5', 'VWO 4', 'VWO 5', 'VWO 6'])
    return frame

def get_titles_meesterproef(data):

    titles_meesterproef = ''

    for index, row in data.iterrows():
        titles_meesterproef = '{0}{1}'.format(titles_meesterproef, row.meesterproef.replace(';', '\n\n'))

    return titles_meesterproef

def get_students_data(data, columns_data):
    '''
    This function shows information from students that are graduated.

    Args:
        data (Pandas.DataFrame): This is the data that will be used for showing information from students that are graduated.
        columns_data (list): columns are the primary keys from the list that will be used in this function.

    Returns:
        dataframe (Pandas.Dataframe): This is a created dataframe that shows the information from students structuredly.
    '''
    students_data = []

    global_env = {}
    local_env = {}

    for school_data in data.data_leerlingen:
        school_data = school_data.replace(';', ',')

        school_data = 'school_data = {}'.format(school_data)
        exec (school_data, global_env, local_env)
        for student in local_env['school_data']:
            students_data.append(student)

    dataframe = pd.DataFrame(data=students_data, columns = columns_data)
    return dataframe

def create_pie_chart(dataframe, column, output_dir, name=None):
    '''
    This function creates a pie chart from a specific column in a DataFrame.

    Args:
        dataframe (Pandas.Dataframe): A dataframe that has to have the specific columns.
        column (string): The name of the specific column.
        output_dir (string): This is the location where the pie chart has to be stored.

    Returns:
        list (list): List with the elements with his percentage from the pie chart.
    '''
    plt.clf()
    try:
        dataframe[column].value_counts().plot(kind='pie')
        plt.axis('equal')
        plt.title('Number of appearances in dataset')
        if name is not None:
            plt.savefig('{0}{1}.png'.format(output_dir,name))
        else:
            plt.savefig('{0}{1}.png'.format(output_dir,column))

        list = []
        count = 0
        for x in dataframe[column].value_counts():
            y = x / dataframe[column].value_counts().sum() * 100
            test = '{0}: {1:.1f}%'.format(dataframe[column].value_counts().index[count], y)
            list.append(test)
            count+=1
    except:
        plt.axis('equal')
        plt.title('Number of appearances in dataset')
        if name is not None:
            plt.savefig('{0}{1}.png'.format(output_dir,name))
        else:
            plt.savefig('{0}{1}.png'.format(output_dir,column))
        list = []

    return list

def create_html(output, data, title_document):
    '''
    This function creates a html file from the data

    Args:
        output (String): the output location plus name of file.
        data (dict<String, List>): Dictionary with as key the name of the object and as value the object.
        name (String): This is the title of the document.
        data (Pandas.DataFrame): This is the data that will be used for calculating the total students of a school or multiple schools.
    '''

    style = '''
<style>
@charset "utf-8";

 html, body {
 margin: 0px;
 padding: 0px;
 border: 0px;
 color: #000;
 background: #fff;
 }
 html, body, p, th, td, li, dd, dt {
 font: 1em Arial, Helvetica, sans-serif;
 }
 h1, h2, h3, h4, h5, h6 {
 font-family: Arial, Helvetica, sans-serif;
 }
 h1 { font-size: 2em; }
 h2 { font-size: 1.5em; }
 h3 { font-size: 1.2em ; }
 h4 { font-size: 1.0em; }
 h5 { font-size: 0.9em; }
 h6 { font-size: 0.8em; }
 a:link { color: #00f; }
 a:visited { color: #009; }
 a:hover { color: #06f; }
 a:active { color: #0cf; }
</style>\n
'''
    markdown = style + '#{}\n'.format(title_document)
    markdown = markdown + '## Inhoud\n'
    for item in data.keys():
        markdown = markdown + '* __{}__\n'.format(item)

    for name, data in data.items():
        markdown = markdown + '\n## {}\n'.format(name)

        i = 0
        for element in data:
            if ('.png' or '.jpg') in str(element):
                #markdown = markdown + '![Image]({} =100x)\n\n'.format(element)
                markdown = markdown + '<img src="{}" width="400">\n\n'.format(element)

            else:
                if type(element) == list:
                    for item in element:
                        markdown = markdown + '{}\n\n'.format(item)

                if type(element) == str:
                    markdown = markdown + '{}\n'.format(element)

                if str(type(element)) == "<class 'pandas.core.frame.DataFrame'>":
                    markdown = markdown + '{}\n'.format(element.to_html())

        i += 1

    with open('{}.html'.format(output), 'w') as file:
        html = markdown2.markdown(markdown)
        markdowner = markdown2.Markdown()
        html = markdowner.convert(html)
        file.write(html)

    return

def create_pdf(output, data, title_document):
    '''
    This function creates a PDF file from the data

    Args:
        output (String): the output location plus name of file.
        data (dict<String, List>): Dictionary with as key the name of the object and as value the object.
        name (String): This is the title of the document.
        data (Pandas.DataFrame): This is the data that will be used for calculating the total students of a school or multiple schools.
    '''
    pdf = fpdf.FPDF(format='letter')
    pdf.add_page()
    pdf.set_font('Arial', 'BU', size=14)
    pdf.multi_cell(200,10, txt=title_document, align='C')
    pdf.ln(1)

    i = 1
    pdf.set_font('Arial', size=12)
    for item in data.keys():
        pdf.cell(0, pdf.font_size, txt='{0}. {1}'.format(i, item), ln=1, align='L')
        i += 1

    for name, data in data.items():
        pdf.add_page()
        pdf.set_font('Arial', 'B', size=13)
        pdf.cell(0,pdf.font_size+10, txt=name, ln=1, align='L')

        i = 0
        for element in data:
            if ('.png' or '.jpg') in str(element):
                pdf.image(data[i], x=10, y=pdf.get_y(), w=100)
                pdf.ln(70)

            else:
                if type(element) == list:
                    for item in data[i]:
                        pdf.set_font('Arial', size=8)
                        pdf.cell(0, pdf.font_size, txt=item, ln=1, align='L')
                        pdf.ln(2)

                if type(element) == str:
                    pdf.set_font('Arial', size=11)
                    pdf.multi_cell(0,pdf.font_size, txt=element, align='L')

            i += 1

    pdf.output('{0}.pdf'.format(output))
    return

def compare_old_to_new(old, new):
    return ((new - old) / old) * 100
