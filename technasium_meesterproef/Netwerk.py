# import dependencies
import pandas as pd
import matplotlib.pyplot as plt
import os
import glob
import fpdf
import helpers

INPUT_PATH = 'input/Netwerk/'
OUTPUT_PATH = 'output/Netwerk/'

# declare primary keys of .csv file
columns = ['naam', 'plaats',
           'tot_ll_1', 'tot_ll_2', 'tot_ll_3', 'tot_ll_4', 'tot_ll_5', 'tot_ll_6',# totaal leerlingen <klas>
           'havo_j_1', 'havo_j_2', 'havo_j_3', 'havo_j_4', 'havo_j_5',            # technasium havo jongens <klas>
           'havo_m_1', 'havo_m_2', 'havo_m_3', 'havo_m_4', 'havo_m_5',            # technasium havo meisjes <klas>
           'vwo_j_1', 'vwo_j_2', 'vwo_j_3', 'vwo_j_4', 'vwo_j_5', 'vwo_j_6',      # technasium havo jongens <klas>
           'vwo_m_1', 'vwo_m_2', 'vwo_m_3', 'vwo_m_4', 'vwo_m_5', 'vwo_m_6',      # technasium havo meisjes <klas>
           'havo_NG_4', 'havo_NG_5',                                              # technasium havo NG <klas>
           'havo_NT_4', 'havo_NT_5',                                              # technasium havo NT <klas>
           'havo_NG_NT_4', 'havo_NG_NT_5',                                        # technasium havo NG-NT <klas>
           'havo_OVERIG_4', 'havo_OVERIG_5',                                      # technasium havo OVERIG <klas>
           'vwo_NG_4', 'vwo_NG_5', 'vwo_NG_6',                                    # technasium vwo NG <klas>
           'vwo_NT_4', 'vwo_NT_5', 'vwo_NT_6',                                    # technasium vwo NT <klas>
           'vwo_NG_NT_4', 'vwo_NG_NT_5', 'vwo_NG_NT_6',                           # technasium vwo NG-NT <klas>
           'vwo_OVERIG_4', 'vwo_OVERIG_5', 'vwo_OVERIG_6',                        # technasium vwo OVERIG <klas>
           'meesterproef',
           'data_leerlingen'
          ]

columns_data = ['niveau', 'onderwijstype', 'onderwijsinstelling', 'studie', 'cluster']

def main(landelijk_total_havo_boys, landelijk_total_havo_girls, landelijk_total_vwo_boys, landelijk_total_vwo_girls, landelijk_total_students_with_cluster_1_2_percentage):
    # load data
    for network in os.listdir(INPUT_PATH):
        try:
            all_files = glob.glob(os.path.join('{0}{1}/'.format(INPUT_PATH, network), "*.csv"))
            df_from_each_file = (pd.read_csv(f, names=columns) for f in all_files)
            concatenated_df   = pd.concat(df_from_each_file, ignore_index=True)
            data = concatenated_df
        except:
            continue

        if not os.path.exists('{0}{1}/'.format(OUTPUT_PATH, network)):
            os.makedirs('{0}{1}/'.format(OUTPUT_PATH, network))

        # get the total technasium students that is stored in a DataFrame
        total_technasium_students_table = helpers.get_total_technasium_students_table(data)

        # get the total technasium students with a certain profile that is stored in a Dataframe
        total_student_profiles = helpers.get_student_profiles_table(data)

        # get information of students that are graduated structuredly
        students_data = helpers.get_students_data(data, columns_data)

        # get titles titles_meesterproef
        titles_meesterproef = helpers.get_titles_meesterproef(data)

        # create pie chart from school and stores it in /output
        pie_chart_school = helpers.create_pie_chart(students_data, 'onderwijsinstelling', '{0}{1}/'.format(OUTPUT_PATH, network))
        pie_chart_profile = helpers.create_pie_chart(students_data, 'onderwijstype', '{0}{1}/'.format(OUTPUT_PATH, network))
        pie_chart_clusters = helpers.create_pie_chart(students_data, 'cluster', '{0}{1}/'.format(OUTPUT_PATH, network))
        pie_chart_clusters_havo = helpers.create_pie_chart(students_data.loc[students_data['niveau'] == 'HAVO'], 'cluster', '{0}{1}/'.format(OUTPUT_PATH, network), name='cluster_havo')
        pie_chart_clusters_vwo = helpers.create_pie_chart(students_data.loc[students_data['niveau'] == 'VWO'], 'cluster', '{0}{1}/'.format(OUTPUT_PATH, network), name='cluster_vwo')

        total_boys_percentage = ((total_technasium_students_table['Totaal havo jongens'].Totaal + total_technasium_students_table['Totaal vwo jongens'].Totaal) / total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
        comparison_total_boys_percentage = helpers.compare_old_to_new(landelijk_total_havo_boys + landelijk_total_vwo_boys, total_boys_percentage)
        total_girls_percentage = ((total_technasium_students_table['Totaal havo meisjes'].Totaal + total_technasium_students_table['Totaal vwo meisjes'].Totaal) / total_technasium_students_table['Totaal technasium leerlingen'].Totaal) * 100
        comparison_total_girls_percentage = helpers.compare_old_to_new(landelijk_total_havo_girls + landelijk_total_vwo_girls, total_girls_percentage)
        total_students_with_cluster_1_2_percentage = (len(students_data.loc[students_data['cluster'] == '1,2'].index) / len(students_data['cluster'].index)) * 100
        comparison_total_students_with_cluster_1_2_percentage = helpers.compare_old_to_new(landelijk_total_students_with_cluster_1_2_percentage, total_students_with_cluster_1_2_percentage)

        '''
        Create a html file with the following chapters:
            - Uitstroom naar vervolgopleidingen
            - Clusters
            - Aantal Technasium studenten
            - Profielen
            - Vergelijking netwerk - landelijk
            - Meesterproef titels
        The title of the document is 'IN-, DOOR- EN UITSTROOMGEGEVENS - NETWERK'
        '''
        helpers.create_html('{0}{1}/{1}'.format(OUTPUT_PATH, network), {
                                        'Uitstroom naar vervolgopleidingen': ['### Onderwijstype', 'onderwijstype.png', pie_chart_profile, '### Onderwijsinstelling', 'onderwijsinstelling.png', pie_chart_school],
                                        'Clusters': ['### Totaal clusters','cluster.png', pie_chart_clusters, '### Clusters havo', 'cluster_havo.png', pie_chart_clusters_havo, '### Clusters VWO', 'cluster_vwo.png', pie_chart_clusters_vwo],
                                        'Aantal Technasium studenten': [total_technasium_students_table],
                                        'Profielen': [total_student_profiles],
                                        'Vergelijking netwerk - landelijk': ['De technasiumleerlingen in dit netwerk bestaan gemiddeld uit {0:.1f}% jongens en uit {1:.1f}% meisjes. Hiermee heeft netwerk {2} gemiddeld {3:.1f}% {4} jongens en {5:.1f}% {6} meisjes t.o.v. de technasiumleerlingen op landelijk niveau.'.format(total_boys_percentage, total_girls_percentage, network, abs(comparison_total_boys_percentage),'meer' if not '-' in str(comparison_total_boys_percentage) else 'minder' , abs(comparison_total_girls_percentage), 'meer' if not '-' in str(comparison_total_girls_percentage) else 'minder'),
                                        'In dit netwerk gaan {0:.1f}% van de leerlingen naar een technische studie. Dit is gemiddeld {1:.1f}% {2} t.o.v. de technasiumleerlingen op landelijk niveau.'.format(total_students_with_cluster_1_2_percentage, abs(comparison_total_students_with_cluster_1_2_percentage), 'meer' if not '-' in str(comparison_total_students_with_cluster_1_2_percentage) else 'minder')],
                                        'Meesterproef titels': [titles_meesterproef]
                                        },
                                        'IN-, DOOR- EN UITSTROOMGEGEVENS - NETWERK: {}'.format(network))

    return total_technasium_students_table, students_data
