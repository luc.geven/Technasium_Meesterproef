from cx_Freeze import setup, Executable
import os

PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')


build_exe_options = {"packages": ["numpy.core._methods", "numpy", "tkinter", "tkinter.filedialog"],
"includes": ["numpy.core._methods", "numpy", "matplotlib.backends.backend_tkagg"],
"include_files": [os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'), os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll')]}
setup(name = 'Technasium',
      version = '1,0',
      description = '',
      options = {"build_exe": build_exe_options},
      executables = [Executable('technasium_meesterproef.py', base='Win32GUI')])
